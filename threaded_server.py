import socket
import threading

###
# FROM https://stackoverflow.com/a/23828265/5712160
###

class ThreadedServer(object):
    def __init__(self, host="0.0.0.0", port=8123, size=1024):
        self.host = host
        self.port = port
        self.size = size

        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind((self.host, self.port))

        self.clients = []
        self.eingaben = []
        self.scores = []

    def listen(self):
        self.sock.listen(10)
        print("Server started!", self.host, self.port, self.size)

        while True:
            if len(self.clients) < 2:
                client, address = self.sock.accept()
                
                self.clients.append(client)
                self.eingaben.append(0)
                self.scores.append(0)

                threading.Thread(target = self.listenToClient, args = (client, address)).start()

    def Spiel(self, W, O):
        if W == O:
            print("Unentschieden")
        elif W == 1 and O == 2:
            print("Schere vs Stein")
            return 2
        elif W == 1 and O == 3:
            print("Schere vs Papier")
            return 1
        elif W == 2 and O == 1:
            print("Stein vs Schere")
            return 1
        elif W == 2 and O == 3:
            print("Stein vs Papier")
            return 2
        elif W == 3 and O == 1:
            print("Papier vs Schere")
            return 2
        elif W == 3 and O == 2:
            print("Papier vs Stein")
            return 1

    def listenToClient(self, client, address):
        print("Connection from " + str(address))
        client.send(("Willkommen bei 'Schere, Stein, Papier'!").encode("utf-8"))

        while True:
            try:
                data = client.recv(self.size)

                if data:
                    msg = data.decode("utf-8")
                    print(str(address) + ": " + msg)
                    
                    try:
                        msg = int(msg)
                    except:
                        continue
                    if msg not in [1, 2, 3]:
                        continue
                    
                    own_index = self.clients.index(client)

                    if self.eingaben[own_index] == 0:
                        self.eingaben[own_index] = msg
                        print(self.eingaben)

                        other_index = 0
                        if own_index == 0:
                            other_index = 1

                        if self.eingaben[other_index] != 0:
                            W = self.eingaben[own_index]
                            O = self.eingaben[other_index]
                            
                            winner = self.Spiel(W, O)
                            
                            Oclient = self.clients[other_index]

                            if winner == 1:
                                self.scores[own_index] += 1
                                print(self.scores)
                                self.send(client, "Du gewinnst!")
                                self.send(Oclient, "Du verlierst!")
                            elif winner == 2:
                                self.scores[other_index] += 1
                                print(self.scores)
                                self.send(client, "Du verlierst!")
                                self.send(Oclient, "Du gewinnst!")
                            else:
                                self.send(self.clients, "Unentschieden!")
                            
                            if self.scores[own_index] >= 3:
                                self.send(client, 1)
                                self.send(Oclient, 2)
                            elif self.scores[other_index] >= 3:
                                self.send(client, 2)
                                self.send(Oclient, 1)
                            else:
                                self.send(self.clients, 0)

                            self.eingaben[own_index] = 0
                            self.eingaben[other_index] = 0
                            print(self.eingaben)
                else:
                    print(str(address) + " disconnected")
                    raise Exception("client disconnected")
            except Exception as e:
                print("Closing connection to " + str(address), e)
                clientindex = self.clients.index(client)

                self.clients.remove(client)
                self.eingaben.pop(clientindex)
                self.scores.pop(clientindex)

                client.close()

                return False
    
    def send(self, clients, msg):
        smsg = str(msg).encode("utf-8")
        if type(clients) == list:
            for cclient in clients:
                cclient.send(smsg)
        else:
            clients.send(smsg)

if __name__ == "__main__":
    host = input("host: ")
    port = 8123
    while True:
        port = input("port: ")
        try:
            port = int(port)
            break
        except ValueError:
            pass

    ThreadedServer(host, port).listen()
